import pandas as pd
import json
import yaml


# read the excel file
def get_excel_sheet_map(excel_sheet_path):
    return pd.read_excel(excel_sheet_path, header=None, sheet_name=None)


#   Get the set of sections
def get_set_of_sections(trash_dataframe):
    section_no_list = list(trash_dataframe.iloc[0].values[1:])
    section_name_list = list(trash_dataframe.iloc[1].values[1:])
    section_full_name_list = [str(section_no_list[i]) + section_name_list[i] for i in range(len(section_no_list))]
    section_set = set(section_full_name_list)
    return section_set


#   Get the set of use_application
def get_set_of_use_application(trash_dataframe):
    set_of_use_application = set(trash_dataframe.iloc[3].values[1:])
    return set_of_use_application


#   Get the list of score of the type of trash
def get_list_of_trash_score(trash_dataframe, section, use_application):  # use_application
    trash_score_2D_list = trash_dataframe.iloc[2:, 1:].values.tolist()
    row_size = len(trash_score_2D_list)
    col_size = len(trash_score_2D_list[0])
    score_list = []
    for column in range(col_size):
        if trash_score_2D_list[0][column] != section or trash_score_2D_list[1][column] != use_application:
            continue
        # if trash_score_2D_list[1][column] != use_application:
        #     continue
        for row in range(2, row_size):
            if isinstance(trash_score_2D_list[row][column], int):
                score_list.append(trash_score_2D_list[row][column])  # add the cell value to the column data list
    return score_list


def process_trash_data(trash_dataframe):
    section_set = get_set_of_sections(trash_dataframe)
    section_use_application_score_list_map_map = {}
    for section in section_set:
        use_application_score_list_map = {}
        use_application_set = get_set_of_use_application(trash_dataframe)
        for use_application in use_application_set:
            trash_score_list = get_list_of_trash_score(trash_dataframe, section, use_application)
            use_application_score_list_map[use_application] = trash_score_list
        section_use_application_score_list_map_map[section] = use_application_score_list_map
    return section_use_application_score_list_map_map


def process_all_sheets():
    sheet_map = get_excel_sheet_map(r'D:\Work\USC\Lab data\R data\temp\test.xlsx')
    for sheet in sheet_map:
        tidy_sheet_data = process_trash_data(sheet_map[sheet])
        write_to_json(tidy_sheet_data, sheet + ".json")
        write_to_yaml(tidy_sheet_data, sheet + ".yaml")


def write_to_json(data, file_name):
    with open("out/" + file_name, "w") as outfile:
        json.dump(data, outfile, indent=2)


def write_to_yaml(data, file_name):
    with open("out/" + file_name, 'w') as file:
        yaml.dump(data, file)


process_all_sheets()
