import pandas as pd

# read the excel file
data = pd.read_excel(r'D:\Work\USC\Lab data\R data\temp\test.xlsx', sheet_name='Sheet1', header=None)


#   Get the set of sections
def get_set_of_sections(trash_dataframe):
    section_no_list = list(trash_dataframe.iloc[0].values[1:])
    section_name_list = list(trash_dataframe.iloc[1].values[1:])
    section_full_name_list = [str(section_no_list[i]) + section_name_list[i] for i in range(len(section_no_list))]
    section_set = set(section_full_name_list)
    return section_set


#   Get the set of use_application
def get_set_of_use_application(trash_dataframe):
    set_of_use_application = set(trash_dataframe.iloc[2].values[1:])
    return set_of_use_application


#   Get the list of score of the type of trash
def get_list_of_trash_score(use_application):
    score_list = ()
    return score_list


#   Put the list into the use_application_score_list_map

def process_trash_data(trash_dataframe):
    section_set = get_set_of_sections(trash_dataframe)
    section_use_application_score_list_map_map = {}
    for section in section_set:
        use_application_score_list_map = {}
        use_application_set = get_set_of_use_application(trash_dataframe)
        for use_application in use_application_set:
            trash_score_list = get_list_of_trash_score(use_application)
            use_application_score_list_map[use_application] = trash_score_list
        section_use_application_score_list_map_map[section] = use_application_score_list_map


process_trash_data(data)
